FROM ubuntu:12.04

RUN apt-get update
RUN apt-get install -y nginx zip curl unzip tar git 

RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN mkdir /var/www && chmod -R 755 /var/www
RUN git clone https://gitlab.com/alexsot/laravel-test.git
RUN cd /var/www/ 
RUN mv laravel-test/* . 
RUN rm -rf laravel-test

EXPOSE 80

CMD ["/usr/sbin/nginx", "-c", "/etc/nginx/nginx.conf"]

FROM shakyshane/laravel-app:latest

WORKDIR /var/www

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \ 
    && chmod 755 composer-setup.php 
RUN php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && php composer.phar install --no-dev --no-scripts \
    && rm composer.phar

COPY . /var/www

RUN chown -R www-data:www-data \
        /var/www/storage \
        /var/www/bootstrap/cache

RUN php artisan optimize

